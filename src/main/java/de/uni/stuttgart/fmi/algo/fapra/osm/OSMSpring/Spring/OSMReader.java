package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class to start the spring boot
 * 
 * @author pacs1
 *
 */

@SpringBootApplication
public class OSMReader {

	public static void main(String[] args) {
		SpringApplication.run(OSMReader.class, args);
	}
}
