package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller class for spring boot
 * 
 * @author pacs1
 *
 */

@Controller
public class MapController {

	@RequestMapping("/")
	public String map() {
		return "mapOrg";
	}

}