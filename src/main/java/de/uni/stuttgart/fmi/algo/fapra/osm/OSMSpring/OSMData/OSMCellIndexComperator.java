package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData;

import java.util.Comparator;

/**
 * Comparator for the cell node map. Sorts the two dimensional array depending
 * on the index of the cells from the smallest to the biggest
 * 
 * @author pacs1
 *
 */

public class OSMCellIndexComperator implements Comparator<int[]> {
	public int compare(int[] o1, int[] o2) {
		// TODO Auto-generated method stub
		if (o1[0] > o2[0]) {
			return 1;
		} else if (o1[0] < o2[0]) {
			return -1;
		} else {
			return 0;
		}
	}

}