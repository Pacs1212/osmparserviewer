package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Spring;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Importer.OSMImporterWithArray;
import de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Importer.OSMTags;
import de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Navigation.OSMDijkstra;
import de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Navigation.OSMTSP;
import de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData.OSMGraph;
import de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData.OSMGrid;
import de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData.OSMGridCoordinatesOutOfBoundException;
import de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData.OSMHamiltonianGraph;

/**
 * Local data access object class. The data are stored on the local machine.
 * 
 * @author pacs1
 *
 */

@Repository
@Qualifier("OSMGraphLocal")
public class OSMGraphLocal implements OSMGraphDAO {

	private static OSMGraph graph = null;
	private static OSMGrid grid = null;
	private static OSMHamiltonianGraph hGraph = null;

	@Override
	public ArrayList<String[]> importGraph(String fileName) {
		String filePath = "inputs/";
		File osmFile = new File(filePath + fileName);
		ArrayList<String[]> imports = new ArrayList<>();
		long startTime = System.currentTimeMillis();
		long importstartTime = System.currentTimeMillis();
		long endTime;

		// Starts the import of the graph
		System.out.println("***************************************************************************");
		System.out.println("Start Graph import");
		importstartTime = System.currentTimeMillis();
		try {
			graph = OSMImporterWithArray.getGraphFromPbfFile(osmFile);
		} catch (FileNotFoundException e) {
			System.out.println("Error while import new graph");
			e.printStackTrace();
		}
		endTime = System.currentTimeMillis();
		System.out.println("Graph import finished: " + (endTime - importstartTime));

		// starts the calculation of the grid
		importstartTime = System.currentTimeMillis();
		grid = new OSMGrid(graph);
		endTime = System.currentTimeMillis();
		System.out.println("Calculate grid: " + (endTime - importstartTime));

		endTime = System.currentTimeMillis();
		System.out.println("Calctime in millis: " + (endTime - startTime));
		System.out.println("Calctime in minutes: " + ((endTime - startTime) / 1000) / 60);
		System.out.println("***************************************************************************");

		// Creates the info output array with all king of nodes and edges which was
		// imported
		// Creates entry for all imported nodes and edges
		String[] tagsNumbers = new String[2];
		tagsNumbers[0] = "Number of edges";
		tagsNumbers[1] = Integer.toString(graph.getNumberOfEdges());
		imports.add(tagsNumbers);
		tagsNumbers = new String[2];
		tagsNumbers[0] = "Number of nodes";
		tagsNumbers[1] = Integer.toString(graph.getNumberOfNodes());
		imports.add(tagsNumbers);

		// Creates the entrys for each kind of node which was imported
		for (String tag : OSMTags.OsmHighwayTags) {
			tagsNumbers = new String[2];
			tagsNumbers[0] = tag;
			tagsNumbers[1] = Integer.toString(graph.getNumberOfTags().get(tag));
			imports.add(tagsNumbers);
		}

		return imports;
	}

	@Override
	public long[][] getAllEdges() {
		return graph.getEdges();
	}

	@Override
	public ArrayList<double[]> getEdgesInBound(double west, double east, double north, double south) {
		boolean[] inside = new boolean[graph.getNumberOfNodes()];
		ArrayList<double[]> edgesToAdd = new ArrayList<>();

		// Checks each node if they are in the bounds and save it to the inside array
		for (int i = 0; i < graph.getNumberOfNodes(); i++) {
			long node = graph.getAllNodes()[i];
			boolean isInside = true;

			double latitude = graph.getNodesLookUp().get(node)[1];
			double longitude = graph.getNodesLookUp().get(node)[2];

			// check longitude
			if (!(longitude >= west && longitude <= east)) {
				isInside = false;
			}

			// check latitude
			if (!(latitude >= south && latitude <= north)) {
				isInside = false;
			}
			inside[i] = isInside;

		}

		// Checks each edge if at least on node is the the bounds
		for (long[] edge : graph.getEdges()) {
			if (inside[(int) graph.getNodesLookUp().get(edge[0])[0]]
					|| inside[(int) graph.getNodesLookUp().get(edge[1])[0]]) {

				// adds edge to the list if on node is the the bounds
				double[] coordinates = new double[4];

				coordinates[0] = graph.getNodesLookUp().get(edge[0])[1];
				coordinates[1] = graph.getNodesLookUp().get(edge[0])[2];
				coordinates[2] = graph.getNodesLookUp().get(edge[1])[1];
				coordinates[3] = graph.getNodesLookUp().get(edge[1])[2];

				edgesToAdd.add(coordinates);

			}
		}

		return edgesToAdd;
	}

	@Override
	public ArrayList<String[]> getBarsInBound(double west, double east, double north, double south) {
		ArrayList<String[]> barCoordsAndName = new ArrayList<>();

		// checks each bar if it is inside the bounds
		for (int barIndex : graph.getBarList()) {
			long node = graph.getAllNodes()[barIndex];
			boolean isInside = true;

			double latitude = graph.getNodesLookUp().get(node)[1];
			double longitude = graph.getNodesLookUp().get(node)[2];

			// check longitude
			if (!(longitude >= west && longitude <= east)) {
				isInside = false;
			}

			// check latitude
			if (!(latitude >= south && latitude <= north)) {
				isInside = false;
			}
			if (isInside) {

				String[] barsInfos = new String[3];

				barsInfos[0] = Double.toString(latitude);
				barsInfos[1] = Double.toString(longitude);
				barsInfos[2] = graph.getBarNameLookUp().get(node);

				barCoordsAndName.add(barsInfos);
			}

		}
		return barCoordsAndName;
	}

	@Override
	public long getNearestNode(double lat, double lng, boolean partyLocation) {
		try {
			long startTime = System.currentTimeMillis();
			long tmp = grid.getNodeFromCoordinate(lat, lng, partyLocation);
			long endTime = System.currentTimeMillis();
			System.out.println("getNearest Node: " + (endTime - startTime));
			return tmp;
		} catch (OSMGridCoordinatesOutOfBoundException e) {
			e.printStackTrace();
		}
		return graph.getAllNodes()[0];
	}

	@Override
	public ArrayList<double[]> getShortestPath(long s, long t) {

		// Get shortest path from the Dijkstra class
		ArrayList<Long> path = OSMDijkstra.calcShotestPath(s, t, graph);
		return getCoordinateListFromPath(path);

	}

	/**
	 * Returns the coordinate list from the given path
	 * 
	 * @param path path from a to b
	 * @return the coordinates in a ordered list from all nodes in the way from a to
	 *         b
	 */
	public ArrayList<double[]> getCoordinateListFromPath(ArrayList<Long> path) {
		ArrayList<double[]> nodeCoorinates = new ArrayList<>();

		// Adds each edge to the arrays of the path
		for (int i = path.size() - 1; i >= 1; i--) {
			double[] coordinates = new double[4];

			coordinates[0] = graph.getNodesLookUp().get(path.get(i))[1];
			coordinates[1] = graph.getNodesLookUp().get(path.get(i))[2];
			coordinates[2] = graph.getNodesLookUp().get(path.get(i - 1))[1];
			coordinates[3] = graph.getNodesLookUp().get(path.get(i - 1))[2];

			nodeCoorinates.add(coordinates);
		}

		return nodeCoorinates;
	}

	@Override
	public double[] getCoorinateFromeNode(long node) {
		return new double[] { graph.getNodesLookUp().get(node)[1], graph.getNodesLookUp().get(node)[2] };
	}

	@Override
	public String getLocationName(long node) {
		return graph.getBarNameLookUp().get(node);
	}

	@Override
	public ArrayList<double[]> calcShortestBarTour(Long[] bars) {
		ArrayList<Long> barEntrances = new ArrayList<>();
		ArrayList<ArrayList<Double>> allShortestWays = new ArrayList<>();
		ArrayList<ArrayList<Long>> neededWays = new ArrayList<>(bars.length);
		HashMap<Long, Long> barStreetMap = new HashMap<>();
		ArrayList<double[]> nodeCoorinates = new ArrayList<>();

		long startTime = System.currentTimeMillis();
		long importstartTime = System.currentTimeMillis();
		long endTime;
		System.out.println("***************************************************************************");
		System.out.println("Start Bar route calculation");

		// searches for the nearest node of a street from the bar and mark them as entry
		// for the tour calculation, because there are no edges between the bars and the
		// street
		importstartTime = System.currentTimeMillis();
		for (long bar : bars) {
			double lat = graph.getNodesLookUp().get(bar)[1];
			double lng = graph.getNodesLookUp().get(bar)[2];
			long streetNode = getNearestNode(lat, lng, false);
			barStreetMap.put(streetNode, bar);
			barEntrances.add(streetNode);
		}

		// checks if the tour was already calculated, if yes reutrn them
		if (hGraph != null && hGraph.checkIfGraphRepresendsNodes(barEntrances)) {
			System.out.println("Route allready exists");
			System.out.println("***************************************************************************");
			return hGraph.getCoordsFromTSP(barEntrances.get(0));
		}
		endTime = System.currentTimeMillis();
		System.out.println("Create entrance nodes: " + (endTime - importstartTime));

		// Calculates a one to many Dijkstra for each bar entrance
		importstartTime = System.currentTimeMillis();
		for (long barEntrance : barEntrances) {
			ArrayList<ArrayList<Double>> shortestWays = OSMDijkstra.calcShotestPaths(barEntrance, barEntrances, graph);
			for (ArrayList<Double> shortestWay : shortestWays) {
				allShortestWays.add(shortestWay);
			}
		}
		endTime = System.currentTimeMillis();
		System.out.println("Dijkstra for form all nodes finshed: " + (endTime - importstartTime));

		// creates a hamiltonian graph out of the bar entrances and the ways between
		// them
		importstartTime = System.currentTimeMillis();
		hGraph = new OSMHamiltonianGraph(barEntrances, allShortestWays, barStreetMap);
		endTime = System.currentTimeMillis();
		System.out.println("Hamiltion Graph creation finshed: " + (endTime - importstartTime));

		// calculates the TSP solution
		importstartTime = System.currentTimeMillis();
		OSMTSP osmTSP = new OSMTSP(hGraph.getGraph(), 0);
		ArrayList<Double> tspPath = osmTSP.solveTSP();
		endTime = System.currentTimeMillis();
		System.out.println("TSP solved: " + (endTime - importstartTime));

		// add the start node to the end of the list for a round trip;
		tspPath.add(0, 0.0);

		// checks which ways was used in the TSP solution by checking the last end first
		// node of each way and look if the are in the TSP are on each other in the
		// right direction
		importstartTime = System.currentTimeMillis();
		for (ArrayList<Double> edge : allShortestWays) {
			Long endBarEdge = edge.get(0).longValue();
			Long startBarEdge = edge.get(edge.size() - 2).longValue();

			for (int i = 0; i < tspPath.size() - 1; i++) {
				Long startBar = barEntrances.get(tspPath.get(i + 1).intValue());
				Long endBar = barEntrances.get(tspPath.get(i).intValue());

				boolean startSame = startBar.equals(startBarEdge);
				boolean endSame = endBar.equals(endBarEdge);

				if (startSame && endSame) {
					edge.remove(edge.size() - 1);
					neededWays.add(convertDoubleEdgeToLongEdge(edge));
				}
			}
		}
		endTime = System.currentTimeMillis();
		System.out.println("Route recaclulation finished: " + (endTime - importstartTime));

		// Get the latitude and longitude of each node in the used ways
		for (ArrayList<Long> edge : neededWays) {
			nodeCoorinates.addAll(getCoordinateListFromPath(edge));
		}

		hGraph.addTSPIfNotExist(tspPath, nodeCoorinates);
		endTime = System.currentTimeMillis();
		System.out.println("Shortest bar tour finished: " + (endTime - startTime));
		System.out.println("***************************************************************************");
		return nodeCoorinates;
	}

	/**
	 * Converts the edge with double values to a edge with long values
	 * 
	 * @param dEdge with double values
	 * @return edge with long values
	 */
	private ArrayList<Long> convertDoubleEdgeToLongEdge(ArrayList<Double> dEdge) {
		ArrayList<Long> lEdge = new ArrayList<>();

		for (int i = 0; i < dEdge.size(); i++) {
			lEdge.add(dEdge.get(i).longValue());
		}

		return lEdge;
	}

	@Override
	public ArrayList<String> getBarTourOrder(Long startBar) {
		double lat = graph.getNodesLookUp().get(startBar)[1];
		double lng = graph.getNodesLookUp().get(startBar)[2];
		long streetNode = getNearestNode(lat, lng, false);

		ArrayList<String> barNames = new ArrayList<>();
		ArrayList<Long> barOrder = hGraph.getTSP(streetNode);

		// get the ordered names of the bars in the TSP solution
		for (int i = barOrder.size() - 1; i >= 0; i--) {
			Long entranceID = hGraph.getBarStreetMap().get(barOrder.get(i));
			barNames.add(getLocationName(entranceID));
		}

		return barNames;
	}

}
