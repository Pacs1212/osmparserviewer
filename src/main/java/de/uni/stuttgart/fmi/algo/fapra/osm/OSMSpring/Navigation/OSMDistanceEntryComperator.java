package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Navigation;

import java.util.Comparator;

public class OSMDistanceEntryComperator implements Comparator<Integer> {

	double[] dist;

	public OSMDistanceEntryComperator(double[] dist) {
		this.dist = dist;
	}

	@Override
	public int compare(Integer arg0, Integer arg1) {
		if (dist[arg0] > dist[arg1]) {
			return 1;
		} else if (dist[arg0] < dist[arg1]) {
			return -1;
		} else {
			return 0;
		}
	}

}