package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Spring;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Controller for web request
 * 
 * @author pacs1
 *
 */

@RestController
//@RequestMapping("/Graph")
public class OSMRestController {

	@Autowired
	OSMGraphService graphService;

	/**
	 * Return all edges which are with at least the one node are in the bound of the
	 * north, east, south and west coordinates.
	 * 
	 * @param west  west bound
	 * @param east  east bound
	 * @param north north bound
	 * @param south south bound
	 * @return all edges which are in the bound with at least one node. [0] is
	 *         latitude, [1] is longitude
	 */
	@RequestMapping(value = "/getEdgesFromCoordinates", method = RequestMethod.GET, produces = "application/json")
	public ArrayList<double[]> getNodesFromCoordinates(@RequestParam(name = "west") double west,
			@RequestParam(name = "east") double east, @RequestParam(name = "north") double north,
			@RequestParam(name = "south") double south) {
		return graphService.getEdgesInBound(west, east, north, south);
	}

	/**
	 * Return all party locations which are in the bound of the north, east, south
	 * and west coordinates.
	 * 
	 * @param west  west bound
	 * @param east  east bound
	 * @param north north bound
	 * @param south south bound
	 * @return all bars which are in the bound area. [0] is latitude, [1] is
	 *         longitude, [2] is the name
	 */
	@RequestMapping(value = "/getBarsInBound", method = RequestMethod.GET, produces = "application/json")
	public ArrayList<String[]> getBarsInBound(@RequestParam(name = "west") double west,
			@RequestParam(name = "east") double east, @RequestParam(name = "north") double north,
			@RequestParam(name = "south") double south) {
		return graphService.getBarsInBound(west, east, north, south);
	}

	/**
	 * return the node which is the nearest to the given coordinates
	 * 
	 * @param lat latitude
	 * @param lng longitude
	 * @return ID of the nearest node
	 */
	@RequestMapping(value = "/getNearestNode", method = RequestMethod.GET, produces = "application/json")
	public long getNearestNode(@RequestParam(name = "lat") double lat, @RequestParam(name = "lng") double lng) {
		return graphService.getNearestNode(lat, lng, false);
	}

	/**
	 * return the party location which is the nearest to the given coordinates
	 * 
	 * @param lat latitude
	 * @param lng longitude
	 * @return ID of the nearest node
	 */
	@RequestMapping(value = "/getNearestBar", method = RequestMethod.GET, produces = "application/json")
	public long getNearestBar(@RequestParam(name = "lat") double lat, @RequestParam(name = "lng") double lng) {
		return graphService.getNearestNode(lat, lng, true);
	}

	/**
	 * Returns the shortest path from the start node to the target node
	 * 
	 * @param s start node
	 * @param t target node
	 * @return shortest path, each double arrays includes the latitude and longitude
	 *         of the nodes of on edge the order is lat1, long1, lat2, lng2
	 */
	@RequestMapping(value = "/getShortestPath", method = RequestMethod.GET, produces = "application/json")
	public ArrayList<double[]> getShortestPath(@RequestParam(name = "start") long start,
			@RequestParam(name = "target") long target) {
		return graphService.getShortestPath(start, target);
	}

	/**
	 * Return the latitude and longitude of the given node
	 * 
	 * @param node node
	 * @return coordinates of the node as array [0] = latitude, [1]=longitude
	 */
	@RequestMapping(value = "/getCoorinateFromeNode", method = RequestMethod.GET, produces = "application/json")
	public double[] getCoorinateFromeNode(@RequestParam(name = "node") long node) {
		return graphService.getCoorinateFromeNode(node);
	}

	/**
	 * Return the name from the location with the given ID
	 * 
	 * @param node node
	 * @return coordinates of the node as array [0] = latitude, [1]=longitude
	 */
	@RequestMapping(value = "/getLocationName", method = RequestMethod.GET, produces = "application/json")
	public String[] getLocationName(@RequestParam(name = "node") long node) {
		return new String[] { graphService.getLocationName(node) };
	}

	/**
	 * Starts the import sequence for an graph
	 * 
	 * @return the list of imported edges and nodes
	 */
	@RequestMapping(value = "/getImportGraph", method = RequestMethod.GET, produces = "application/json")
	public ArrayList<String[]> getImportGraph(@RequestParam(name = "fileName") String fileName) {
		return graphService.importGraph(fileName);
	}

	/**
	 * Calculates the shortest path between all Bars
	 * 
	 * @param bars bars to calculate the tour for
	 * @return sorted list of the coordinates
	 */
	@RequestMapping(value = "/calcShortestBarTour", method = RequestMethod.GET, produces = "application/json")
	public ArrayList<double[]> calcShortestBarTour(@RequestParam(name = "bars") Long[] bars) {
		return graphService.calcShortestBarTour(bars);
	}

	/**
	 * Get the order of the bar tour from the given bar ore null with no calculation
	 * from this bar starts
	 * 
	 * @param startBar
	 * @return bar order
	 */
	@RequestMapping(value = "/getBarToureOrder", method = RequestMethod.GET, produces = "application/json")
	public ArrayList<String> getBarTourOrder(@RequestParam(name = "startBar") Long startBar) {
		return graphService.getBarTourOrder(startBar);
	}

}
