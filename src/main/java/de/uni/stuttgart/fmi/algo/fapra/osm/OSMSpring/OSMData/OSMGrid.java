
package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData;

import java.util.Arrays;

/**
 * Places a grid over the graph for better interaction
 * 
 * @author pacs1
 *
 */

public class OSMGrid {
	private OSMGraph g;
	private int[][] nodeCellMap;
	private int[] nodesSorted;
	private int[] cellIndex;
	private int[] gridOffset;
	private double lat_min;
	private double lng_min;
	private int gridHeight;
	private int gridWidth;
	private double cellSize;;

	/**
	 * Creates a grid with the default size of ~25km
	 * 
	 * @param graph to place to grid on
	 */
	public OSMGrid(OSMGraph graph) {
		super();
		this.g = graph;
		this.cellSize = 0.025;
		createArrays();
		createCellsBounds();
		createNodeCellIndexArray();
		createOffsetArray();
	}

	/**
	 * Creates a grid over the given graph with the given size.
	 * 
	 * @param graph    to place to grid on
	 * @param gridsize of each cell in the grid. The value has to be between >0 and
	 *                 1. 0.1 equates ~12km and 1 equates ~110km
	 */
	public OSMGrid(OSMGraph graph, double gridsize) {
		super();
		this.cellSize = gridsize;
		this.g = graph;
		createArrays();
		createCellsBounds();
		createNodeCellIndexArray();
		createOffsetArray();
	}


	/**
	 * Decelerates all variables with the default values;
	 */
	private void createArrays() {
		nodeCellMap = new int[g.getAllNodes().length][2];
		cellIndex = new int[g.getAllNodes().length];
		nodesSorted = new int[g.getAllNodes().length];
	}

	/**
	 * Creates the bounds of each cell in the grid
	 */
	private void createCellsBounds() {

		double southBound = g.getSouthBound();
		double eastBound = g.getEastBound();
		double northBound = g.getNorthBound();
		double westBound = g.getWestBound();

		// calculates the width and the high of each the total grid
		double width = Math.abs(eastBound - westBound) / cellSize;
		double height = Math.abs(southBound - northBound) / cellSize;

		lat_min = (northBound < southBound) ? northBound : southBound;
		lng_min = (westBound < eastBound) ? westBound : eastBound;

		gridWidth = getTotalValues(width);
		gridHeight = getTotalValues(height);
		int numberOfCells = gridWidth * gridHeight;

		// Decelerates the arrays depending on the number of cells in the grids;
		gridOffset = new int[numberOfCells];

	}

	/**
	 * Calculates which node is in which cell in the grid and save the indexes in
	 * the nodeGridMap
	 */
	private void createNodeCellIndexArray() {

		long[] nodes = g.getAllNodes();

		// Checks each node in which cell it depends
		for (int i = 0; i < nodes.length; i++) {
			double latitude = g.getNodesLookUp().get(nodes[i])[1];
			double longitude = g.getNodesLookUp().get(nodes[i])[2];

			int cellIndex = getCellFromCoordinates(latitude, longitude);

			if (cellIndex == -1) {
				System.err.println("Knoten Konnte nicht zugeordnet werden!");
			} else {
				nodeCellMap[i][0] = cellIndex;
				nodeCellMap[i][1] = i;
			}

		}

		// Sorts the array depending on the cell number
		Arrays.sort(nodeCellMap, new OSMCellIndexComperator());

		// create single arrays out of the map for better performance
		for (int i = 0; i < nodeCellMap.length; i++) {
			cellIndex[i] = nodeCellMap[i][0];
			nodesSorted[i] = nodeCellMap[i][1];
		}

		// Set to null for less space usage
		nodeCellMap = null;
	}

	/**
	 * Creates the offset arrays for each cell
	 */

	private void createOffsetArray() {
		int currentCell = 0;
		gridOffset[0] = 0;

		for (int i = 0; i < cellIndex.length; i++) {
			int tmpCell = cellIndex[i];
			if (tmpCell != currentCell) {
				currentCell = tmpCell;
				gridOffset[cellIndex[i]] = i;
			}
		}
	}

	/**
	 * Returns the node which is the nearest to the given coordinates.
	 * 
	 * @param lat           latitude of the given point
	 * @param lng           longitude of the given point
	 * @param partyLocation true when the node should be a party location
	 * @return node which is the nearest to the given point
	 * @throws OSMGridCoordinatesOutOfBoundException
	 */
	public long getNodeFromCoordinate(double lat, double lng, boolean partyLocation)
			throws OSMGridCoordinatesOutOfBoundException {
		// Get grid in which the point is
		int cellIndex = getCellFromCoordinates(lat, lng);

		// Throws exception if the coordinates are out of the grid
		if (cellIndex == -1) {
			throw new OSMGridCoordinatesOutOfBoundException();
		}

		// Get the x and y coordinates of the cell in which the point is
		int xCord = (cellIndex % gridWidth);
		int yCord = ((int) cellIndex / gridWidth);

		double minDistance = Double.MAX_VALUE;
		long nearstNode = 0;

		// Checks each node in the cells which borders to the cell with the point in it
		// to get the nearest node.
		for (int y = yCord - 1; y <= yCord + 1; y++) {
			for (int x = xCord - 1; x <= xCord + 1; x++) {
				if (y >= 0 && y <= gridHeight - 1 && x >= 0 && x <= gridWidth - 1) {
					int currentCellIndex = y * gridWidth + x;

					// Get the nearest node in the cell
					int tmpNodeIndex = getNearstNodeInCell(lat, lng, currentCellIndex, partyLocation);
					long tmpNode = g.getAllNodes()[tmpNodeIndex];

					double nodeLat = g.getNodesLookUp().get(tmpNode)[1];
					double nodeLng = g.getNodesLookUp().get(tmpNode)[2];

					// Checks the distance between the node and the point
					double tmpDist = distanceBetweenToNodes(lat, nodeLat, lng, nodeLng);

					// Set a new nearest node if it closer then the current one
					if (tmpDist < minDistance) {
						minDistance = tmpDist;
						nearstNode = tmpNode;
					}
				}
			}
		}

		return nearstNode;
	}

	/**
	 * Returns the nearest node from the given point in the given cell
	 * 
	 * @param lat           latitude of the given point
	 * @param lng           longitude of the given point
	 * @param partyLocation true when the node should be a party location
	 * @param cellIndex     cell to check
	 * @return index in the node array of the nearest node
	 */
	private int getNearstNodeInCell(double lat, double lng, int cellIndex, boolean partyLocation) {
		int nodeIndex = gridOffset[cellIndex];
		int nearestNodeID = 0;
		double currentDist = Double.MAX_VALUE;
		int lastNodeIndex;

		// Sets the value of how many nodes have to be check. Depended on the index of
		// the cell and the grid offset length.
		if ((cellIndex + 1) < gridOffset.length) {
			lastNodeIndex = gridOffset[cellIndex + 1] - 1;
		} else {
			lastNodeIndex = nodesSorted.length - 1;
		}

		// Checks each node in the cell if it the closest to the point
		while (nodeIndex <= lastNodeIndex) {
			// checks is a party location ore a other node should be returned
			if (partyLocation) {

				// checks if the current node is not a party location
				if (g.getBarList().contains(nodesSorted[nodeIndex])) {
					long nodeID = g.getAllNodes()[nodesSorted[nodeIndex]];
					double nodeLat = g.getNodesLookUp().get(nodeID)[1];
					double nodeLng = g.getNodesLookUp().get(nodeID)[2];

					double tmpDist = distanceBetweenToNodes(lat, nodeLat, lng, nodeLng);

					// set new nearest node
					if (tmpDist < currentDist) {
						currentDist = tmpDist;
						nearestNodeID = nodesSorted[nodeIndex];
					}
				}

			} else {

				// checks if the current node is not a party location
				if (!g.getBarList().contains(nodesSorted[nodeIndex])) {
					long nodeID = g.getAllNodes()[nodesSorted[nodeIndex]];
					double nodeLat = g.getNodesLookUp().get(nodeID)[1];
					double nodeLng = g.getNodesLookUp().get(nodeID)[2];

					double tmpDist = distanceBetweenToNodes(lat, nodeLat, lng, nodeLng);

					// set new nearest node
					if (tmpDist < currentDist) {
						currentDist = tmpDist;
						nearestNodeID = nodesSorted[nodeIndex];
					}
				}
			}
			nodeIndex++;
		}
		return nearestNodeID;
	}

	/**
	 * Returns the cell in which the point are in
	 * 
	 * @param lat latitude of the given point
	 * @param lng longitude of the given point
	 * @return cell index of the cell in which is the point ore -1 if the point is
	 *         not in the grid
	 */
	private int getCellFromCoordinates(double lat, double lng) {

		if (lng >= g.getWestBound() && lng <= g.getEastBound() && lat >= g.getSouthBound()
				&& lat <= g.getNorthBound()) {
			double lat_dif = lat - lat_min;
			double lng_dif = lng - lng_min;

			if (lng_dif == 0) {
				lng_dif = cellSize / 2;
			}
			if (lat_dif == 0) {
				lat_dif = cellSize / 2;
			}

			int y = (int) Math.ceil(lat_dif / cellSize);
			int x = (int) Math.ceil(lng_dif / cellSize);

			return (gridHeight - y) * gridWidth + x;
		}
		return -1;

	}

	/**
	 * Calculates the distance between to nodes in meters
	 * 
	 * @param lat1 latitude of the first node
	 * @param lat2 latitude of the second node
	 * @param lng1 longitude of the first node
	 * @param lng2 longitude of the second node
	 * @return distance in meter between the two nodes
	 */
	private static long distanceBetweenToNodes(double lat1, double lat2, double lng1, double lng2) {

		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lng2 - lng1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		return Math.round(distance);
	}

	private int getTotalValues(double d) {
		if (d % 1 == 0) {
			return (int) d;
		} else {
			return (int) d + 1;
		}
	}

}
