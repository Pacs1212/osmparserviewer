package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Spring;

import java.util.ArrayList;

/**
 * Data Access object interface for the OSM graph. There can be classes for
 * local data storage, different databases ore other types.
 * 
 * @author pacs1
 *
 */

public interface OSMGraphDAO {

	/**
	 * Returns all edges from the graph
	 * 
	 * @return all edges from the graph
	 */
	long[][] getAllEdges();

	/**
	 * Return all edges which are with at least the one node are in the bound of the
	 * north, east, south and west coordinates.
	 * 
	 * @param west  west bound
	 * @param east  east bound
	 * @param north north bound
	 * @param south south bound
	 * @return all edges which are in the bound with at least one node. [0] is
	 *         latitude, [1] is longitude
	 */
	ArrayList<double[]> getEdgesInBound(double west, double east, double north, double south);

	/**
	 * Return all party locations which are in the bound of the north, east, south
	 * and west coordinates.
	 * 
	 * @param west  west bound
	 * @param east  east bound
	 * @param north north bound
	 * @param south south bound
	 * @return all bars which are in the bound area. [0] is latitude, [1] is
	 *         longitude, [2] is the name
	 */
	public ArrayList<String[]> getBarsInBound(double west, double east, double north, double south);

	/**
	 * return the node which is the nearest to the given coordinates
	 * 
	 * @param lat           latitude
	 * @param lng           longitude
	 * @param partyLocation true when the node should be a party location
	 * @return ID of the nearest node
	 */
	long getNearestNode(double lat, double lng, boolean partyLocation);

	/**
	 * Return the name from the location with the given ID
	 * 
	 * @param node node
	 * @return coordinates of the node as array [0] = latitude, [1]=longitude
	 */
	public String getLocationName(long node);

	/**
	 * Returns the shortest path from the start node to teh target node
	 * 
	 * @param s start node
	 * @param t target node
	 * @return shortest path, each double arrays includes the latitude and longitude
	 *         of the nodes of on edge the order is lat1, long1, lat2, lng2
	 */
	public ArrayList<double[]> getShortestPath(long s, long t);

	/**
	 * Return the latitude and longitude of the given node
	 * 
	 * @param node node
	 * @return coordinates of the node as array [0] = latitude, [1]=longitude
	 */
	public double[] getCoorinateFromeNode(long node);

	/**
	 * Starts the import sequence for an graph
	 * 
	 * @return the list of imported edges and nodes
	 */

	public ArrayList<String[]> importGraph(String fileName);

	/**
	 * Calculates the shortest path between all Bars
	 * 
	 * @param bars bars to calculate the tour for
	 * @return sorted list of the coordinates
	 */
	public ArrayList<double[]> calcShortestBarTour(Long[] bars);

	/**
	 * Get the order of the bar tour from the given bar ore null with no calculation
	 * from this bar starts
	 * 
	 * @param startBar
	 * @return bar order
	 */
	public ArrayList<String> getBarTourOrder(Long startBar);

}
