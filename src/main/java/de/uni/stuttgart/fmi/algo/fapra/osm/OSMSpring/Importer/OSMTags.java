package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Importer;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Lists all tags from the osm data which should be imported
 * @author pacs1
 *
 */

public class OSMTags {

	public static final ArrayList<String> OsmHighwayTags = new ArrayList<String>(
			Arrays.asList("motorway", "motorway_link", "primary", "primary_link", "secondary", "secondary_link",
					"tertiary", "tertiary_link", "trunk", "trunk_link", "unclassified", "residential", "living_street",
					"road", "service", "turning_circle"));
	
	public static final ArrayList<String> OsmPartyTags = new ArrayList<String>(
			Arrays.asList("bar","pub","nightclub","biergarten","cafe","restaurant","stripclub","swingerclub"));

}
