package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Graph from the imported pbf file
 * 
 * @author pacs1
 *
 */

public class OSMGraph {

	private HashMap<Long, double[]> nodesLookUp;
	private HashMap<String, Integer> numberOfTags;
	private long[][] edges;
	private int[] edgesFrom;
	private int[] edgesTo;
	private double[] edgeWight;
	private long[] nodes;
	private int[] nodeOffset;
	private double southBound;
	private double eastBound;
	private double northBound;
	private double westBound;

	// Variables for bar navigation
	private HashMap<Long, String> barNameLookUp;
	private ArrayList<Integer> barList;

	public OSMGraph(HashMap<Long, double[]> nodesLookUp, long[][] edges, long[] nodes, int[] nodeOffset,
			double southBound, double eastBound, double northBound, double westBound,
			HashMap<String, Integer> numberOfTags, ArrayList<Integer> barList, HashMap<Long, String> barNameLookUp) {
		super();
		this.nodesLookUp = nodesLookUp;
		this.edges = edges;
		this.nodes = nodes;
		this.nodeOffset = nodeOffset;
		this.southBound = southBound;
		this.eastBound = eastBound;
		this.northBound = northBound;
		this.westBound = westBound;
		this.numberOfTags = numberOfTags;
		this.barList = barList;
		this.barNameLookUp = barNameLookUp;
	}

	public HashMap<Long, double[]> getNodesLookUp() {
		return nodesLookUp;
	}

	public long[][] getEdges() {
		return edges;
	}

	public long[] getAllNodes() {
		return nodes;
	}

	public int[] getNodeOffset() {
		return nodeOffset;
	}

	public int getNumberOfNodes() {
		return nodes.length;
	}

	public int getNumberOfEdges() {
		return edges.length;
	}

	public int[] getEdgesFrom() {
		return edgesFrom;
	}

	public void setEdgesFrom(int[] edgesFrom) {
		this.edgesFrom = edgesFrom;
	}

	public int[] getEdgesTo() {
		return edgesTo;
	}

	public void setEdgesTo(int[] edgesTo) {
		this.edgesTo = edgesTo;
	}

	public double[] getEdgeWight() {
		return edgeWight;
	}

	public void setEdgeWight(double[] edgeWight) {
		this.edgeWight = edgeWight;
	}

	public double getSouthBound() {
		return southBound;
	}

	public double getEastBound() {
		return eastBound;
	}

	public double getNorthBound() {
		return northBound;
	}

	public double getWestBound() {
		return westBound;
	}

	public HashMap<String, Integer> getNumberOfTags() {
		return numberOfTags;
	}

	public HashMap<Long, String> getBarNameLookUp() {
		return barNameLookUp;
	}

	public ArrayList<Integer> getBarList() {
		return barList;
	}

	/**
	 * Returns the weigth of the edge from s to t
	 * 
	 * @param s start node id
	 * @param t target node id
	 * @return edge weight if exist ore -1 else.
	 */
	public double getOneEdgeWeight(int s, int t) {
		double weight = -1;

		int edgeIndex = nodeOffset[s];

		while (edgeIndex < edgesFrom.length && edgesFrom[edgeIndex] == s) {
			if (edgesTo[edgeIndex] == t) {
				return edgeWight[edgeIndex];
			}
			edgeIndex++;
		}

		return weight;

	}

}
