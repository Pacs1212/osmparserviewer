package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData;

/**
 * Exception for the grid, if the coordinates are out of the bounds of the grid
 * 
 * @author pacs1
 *
 */
public class OSMGridCoordinatesOutOfBoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public OSMGridCoordinatesOutOfBoundException() {
		super("Coordinates are out of the Grid");
	}
}
