package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Creates an hamiltonian graph represented with a adjacency matrix.
 * 
 * @author pacs1
 *
 */

public class OSMHamiltonianGraph {
	private double[][] graph;
	private ArrayList<Long> nodes;
	private ArrayList<ArrayList<Double>> edges;
	private HashMap<Long, ArrayList<Long>> tspMap;
	private HashMap<Long, ArrayList<double[]>> tspMapCoord;
	private HashMap<Long, Long> barStreetMap;

	/**
	 * Creates an hamiltonian graph represented with a adjacency matrix. The given
	 * nodes a re nodes of the graph, the edges are all edges between the nodes.
	 * Nodes in these edges can be ignored for the creation of the hamiltonian
	 * graph, because only the sum of weight of the edges are imported for the graph
	 * creation.
	 * 
	 * @param nodes nodes which should be in the graph
	 * @param edges edges between all nodes for an complete graph
	 */
	public OSMHamiltonianGraph(ArrayList<Long> nodes, ArrayList<ArrayList<Double>> edges,
			HashMap<Long, Long> barStreetMap) {
		this.nodes = nodes;
		this.edges = edges;
		this.tspMap = new HashMap<>();
		this.tspMapCoord = new HashMap<>();
		this.barStreetMap = barStreetMap;
		createGraph();
		System.out.println("Hamiltion Graph finished");
	}

	private void createGraph() {
		graph = new double[nodes.size()][nodes.size()];

		for (int i = 0; i < graph.length; i++) {
			for (int j = 0; j < graph[i].length; j++) {
				graph[i][j] = 0;
			}
		}

		for (ArrayList<Double> edge : edges) {
			int indexTo = nodes.indexOf(edge.get(0).longValue());
			int indexFrom = nodes.indexOf(edge.get(edge.size() - 2).longValue());
			double weight = edge.get(edge.size() - 1);

			graph[indexFrom][indexTo] = weight;
		}

	}

	/**
	 * Hamiltonian Graph as adjacency matrix with weights
	 * 
	 * @return adjacency matrix with weights
	 */
	public double[][] getGraph() {
		return graph;
	}

	/**
	 * Nodes in the hamiltonian graph
	 * 
	 * @return list of nodes
	 */
	public ArrayList<Long> getNodes() {
		return nodes;
	}

	/**
	 * All the list of edges between all nodes
	 * 
	 * @return list of edges
	 */
	public ArrayList<ArrayList<Double>> getEdges() {
		return edges;
	}

	/**
	 * Returns the map of the entrance of the bar node an the bar node
	 * 
	 * @return barStreetMap from entrance to bar
	 */
	public HashMap<Long, Long> getBarStreetMap() {
		return barStreetMap;
	}

	/**
	 * Checks if the current graph is the graph from the given node list
	 * 
	 * @param nodesToCheck list of nodes to check if the graph represents them
	 * @return is true the graph represents them, false otherwise
	 */
	public boolean checkIfGraphRepresendsNodes(ArrayList<Long> nodesToCheck) {
		return (this.nodes.containsAll(nodesToCheck) && this.nodes.size() == nodesToCheck.size());
	}

	/**
	 * Returns the TSP route from the given start node if one exists otherwise it
	 * will return null;
	 * 
	 * @param startNode start node from the TSP
	 * @return result from the tsp with given start node
	 */
	public ArrayList<Long> getTSP(Long startNode) {
		if (tspMap.containsKey(startNode)) {
			return tspMap.get(startNode);
		}
		return null;
	}

	/**
	 * Returns the TSP route coordinates from the given start node if one exists
	 * otherwise it will return null;
	 * 
	 * @param startNode start node from the TSP
	 * @return path coordinates fromt he tsp
	 */
	public ArrayList<double[]> getCoordsFromTSP(Long startNode) {
		if (tspMapCoord.containsKey(startNode)) {
			return tspMapCoord.get(startNode);
		}
		return null;
	}

	/**
	 * Adds a tsp path to the map if it not already exists
	 * 
	 * @param tspPath path to add
	 */
	public void addTSPIfNotExist(ArrayList<Double> tspPath, ArrayList<double[]> coords) {
		Long startNode = nodes.get(tspPath.get(0).intValue());
		if (!tspMap.containsKey(startNode)) {
			tspMap.put(startNode, createNodeTSPPath(tspPath));
			tspMapCoord.put(startNode, coords);
		}
	}

	/**
	 * Creates a list out of nodes with the given list of IDs
	 * 
	 * @param tspPath id list
	 * @return node list
	 */
	private ArrayList<Long> createNodeTSPPath(ArrayList<Double> tspPath) {
		ArrayList<Long> nodeTSPPath = new ArrayList<>();

		for (Double nodeID : tspPath) {
			nodeTSPPath.add(nodes.get(nodeID.intValue()));

		}
		return nodeTSPPath;
	}

}
