package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Importer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import de.topobyte.osm4j.core.access.OsmIterator;
import de.topobyte.osm4j.core.model.iface.EntityContainer;
import de.topobyte.osm4j.core.model.iface.EntityType;
import de.topobyte.osm4j.core.model.iface.OsmNode;
import de.topobyte.osm4j.core.model.impl.Way;
import de.topobyte.osm4j.pbf.seq.PbfIterator;
import de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData.OSMGraph;

/**
 * Creates a Graph out of the given pbf file. Stores the nodes and edges in
 * arrays.
 * 
 * @author pacs1
 *
 */

public class OSMImporterWithArray {
	// key is the ID of the node. Values are the [0]=index in the node array;
	// [1]=latitude; [2]=longitude
	private static HashMap<Long, double[]> nodesLookUp;
	private static HashMap<String, Integer> numberOfTags;
	private static ArrayList<Long[]>edges;
	private static ArrayList<Long> nodes;
	private static int[] nodeOffset = null;
	private static int numberOfEdges, numberOfNodes;
	private static double southBound;
	private static double eastBound;
	private static double northBound;
	private static double westBound;

	// Variables for bar navigation
	private static HashMap<Long, String> barNameLookUp;
	private static ArrayList<Integer> barList;

	/**
	 * Creates OSMGraph out of the given file
	 * 
	 * @param pbfFile file to import data from
	 * @return OSMGraph
	 * @throws FileNotFoundException
	 */
	public static OSMGraph getGraphFromPbfFile(File pbfFile) throws FileNotFoundException {

		long startTime;
		long endTime;

		startTime = System.currentTimeMillis();
		declareDeafultValues();
		endTime = System.currentTimeMillis();
		System.out.println("declare variables: " + (endTime - startTime));

		startTime = System.currentTimeMillis();
		getAllEdgesFormContainer(pbfFile);
		endTime = System.currentTimeMillis();
		System.out.println("edge import finished: " + (endTime - startTime));

		startTime = System.currentTimeMillis();
		getAllNodesFormContainer(pbfFile);
		endTime = System.currentTimeMillis();
		System.out.println("NodeLookUp finished: " + (endTime - startTime));

		startTime = System.currentTimeMillis();
		calcEdgeWeights();
		endTime = System.currentTimeMillis();
		System.out.println("Edge wight finished: " + (endTime - startTime));
		
		startTime = System.currentTimeMillis();		
		long[] nodeArray = new long[nodes.size()];
		
		for(int i=0;i<nodes.size();i++) {
			nodeArray[i] = nodes.get(i).longValue();
		}
		endTime = System.currentTimeMillis();
		System.out.println("write all nodes in Array: " + (endTime - startTime));
		
		startTime = System.currentTimeMillis();		
		long[][] edgeArray = new long[edges.size()][3];
		
		for(int i=0;i<edges.size();i++) {
			edgeArray[i][0] = edges.get(i)[0].longValue();
			edgeArray[i][1] = edges.get(i)[1].longValue();
			edgeArray[i][2] = edges.get(i)[2].longValue();
		}
		endTime = System.currentTimeMillis();
		System.out.println("write all edges in Array: " + (endTime - startTime));		
		
		edges.clear();
		nodes.clear();
		

		startTime = System.currentTimeMillis();
		createOffSetArray(edgeArray);
		endTime = System.currentTimeMillis();
		System.out.println("offset Array finished: " + (endTime - startTime));
		

		OSMGraph osmGraph = new OSMGraph(nodesLookUp, edgeArray, nodeArray, nodeOffset, southBound, eastBound, northBound,
				westBound, numberOfTags, barList, barNameLookUp);


		startTime = System.currentTimeMillis();
		createFromToEdgeArrays(osmGraph, edgeArray);
		endTime = System.currentTimeMillis();
		System.out.println("create from to edge arrays: " + (endTime - startTime));

		System.out.println("Number of Nodes: " + (numberOfNodes));
		System.out.println("Number of Edges: " + (numberOfEdges));
		System.out.println("Number of Bars: " + (barNameLookUp.keySet().size()));

		return osmGraph;
	}

	/**
	 * Decelerates all variables with the default values;
	 */
	private static void declareDeafultValues() {
		edges = new ArrayList<Long[]>();
		nodes = new ArrayList<Long>();

		numberOfEdges = 0;
		numberOfNodes = 0;

		nodesLookUp = new HashMap<>();
		barList = new ArrayList<>();
		barNameLookUp = new HashMap<>();

		numberOfTags = new HashMap<>();

		for (String tag : OSMTags.OsmHighwayTags) {
			numberOfTags.put(tag, 0);
		}

		southBound = Double.MAX_VALUE;
		northBound = Double.MIN_VALUE;
		eastBound = Double.MIN_VALUE;
		westBound = Double.MAX_VALUE;
	}

	/**
	 * Imports all ways from the pbf file
	 * 
	 * @param pbfFile pbf file from which the edges are imported
	 * @throws FileNotFoundException
	 */
	private static void getAllEdgesFormContainer(File pbfFile) throws FileNotFoundException {
		OsmIterator iterator = new PbfIterator(new FileInputStream(pbfFile), true);

		for (EntityContainer entityContainer : iterator) {
			// Checks if the current container is a way
			if (entityContainer.getType().equals(EntityType.Way)) {
				createNodeArrayOutOfWay((Way) entityContainer.getEntity());
			}
		}
	}

	/**
	 * Imports all edges from the pbf file
	 * 
	 * @param pbfFile pbf file from which the nodes are imported
	 * @throws FileNotFoundException
	 */
	private static void getAllNodesFormContainer(File pbfFile) throws FileNotFoundException {
		OsmIterator iterator = new PbfIterator(new FileInputStream(pbfFile), true);
		double lng;
		double lat;
		;

		for (EntityContainer entityContainer : iterator) {

			// Checks if the container is an node
			if (entityContainer.getType().equals(EntityType.Node)) {

				OsmNode currentNode = (OsmNode) entityContainer.getEntity();

				// checks if the node is in the node lookup
				if (nodesLookUp.containsKey(currentNode.getId())) {
					lat = currentNode.getLatitude();
					lng = currentNode.getLongitude();

					// add the longitude and latitude to the lookup
					nodesLookUp.get(currentNode.getId())[1] = lat;
					nodesLookUp.get(currentNode.getId())[2] = lng;

					if (lat > northBound) {
						northBound = lat;
					}
					if (lat < southBound) {
						southBound = lat;
					}
					if (lng < westBound) {
						westBound = lng;
					}
					if (lng > eastBound) {
						eastBound = lng;
					}
					continue;
				}

				// adds the party nodes to the list and the lookup
				for (int i = 0; i < currentNode.getNumberOfTags(); i++) {
					String tagValue = currentNode.getTag(i).getValue();

					// checks if the current node is a party node
					if (currentNode.getTag(i).getKey().equals("amenity") && OSMTags.OsmPartyTags.contains(tagValue)) {
						String name = "noNameGiven";

						// search for the name of the node
						for (int j = 0; j < currentNode.getNumberOfTags(); j++) {
							if (currentNode.getTag(j).getKey().equals("name")) {
								name = currentNode.getTag(j).getValue();
							}
						}
						
						barNameLookUp.put(currentNode.getId(), name);
						addNode(currentNode.getId(), currentNode.getLatitude(), currentNode.getLongitude());
						barList.add(numberOfNodes-1);

					}

				}

			}
		}
	}

	/**
	 * save the way as single edges into the edge array
	 * 
	 * @param way way to import
	 */
	private static void createNodeArrayOutOfWay(Way way) {

		for (int i = 0; i < way.getNumberOfTags(); i++) {

			// checks if the way should be imported by the saved tag list
			if (way.getTag(i).getKey().equals("highway") && OSMTags.OsmHighwayTags.contains(way.getTag(i).getValue())) {
				String tagValue = way.getTag(i).getValue();
				numberOfTags.put(tagValue, numberOfTags.get(tagValue) + 1);
				/*
				 * depend of the direction of the way the import of edges is different 1:
				 * forward 0: both directions -1: backward
				 */
				switch (getWayDirection(way)) {
				case 1:

					// Import forward edges of the way and set default edge weight
					for (int j = 0; j < way.getNumberOfNodes() - 1; j++) {
						Long[] tmpArray = new Long[3];
						tmpArray[0] = way.getNodeId(j);
						tmpArray[1] = way.getNodeId(j + 1);
						tmpArray[2] = (long) -1;
						edges.add(tmpArray);
						numberOfEdges += 1;

						// add node to the array and lookup
						addNode(way.getNodeId(j));

					}

					// add the last node from the way to the array and lookup
					addNode(way.getNodeId(way.getNumberOfNodes() - 1));
					break;
				case 0:
					// Import forward and backward edges of the way and set default edge weight
					for (int j = 0; j < way.getNumberOfNodes() - 1; j++) {
						Long[] tmpArray = new Long[3];
						tmpArray[0] = way.getNodeId(j);
						tmpArray[1] = way.getNodeId(j + 1);
						tmpArray[2] = (long) -1;
						edges.add(tmpArray);
						numberOfEdges += 1;

						tmpArray = new Long[3];
						tmpArray[0] = way.getNodeId(j + 1);
						tmpArray[1] = way.getNodeId(j);
						tmpArray[2] = (long) -1;
						edges.add(tmpArray);
						numberOfEdges += 1;

						// add node to the array and lookup
						addNode(way.getNodeId(j));
					}

					// add the last node from the way to the array and lookup
					addNode(way.getNodeId(way.getNumberOfNodes() - 1));
					break;
				case -1:
					// Import backward edges of the way and set default edge weight
					for (int j = 0; j < way.getNumberOfNodes() - 1; j++) {
						Long[] tmpArray = new Long[3];
						tmpArray[0] = way.getNodeId(j + 1);
						tmpArray[1] = way.getNodeId(j);
						tmpArray[2] = (long) -1;
						edges.add(tmpArray);
						numberOfEdges += 1;

						// add node to the array and lookup
						addNode(way.getNodeId(j));
					}

					// add the last node from the way to the array and lookup
					addNode(way.getNodeId(way.getNumberOfNodes() - 1));
					break;

				default:
					break;
				}

				return;
			}
		}
	}

	/**
	 * Return the direction of the given way as Integer. 1 = forward 0 = both -1 =
	 * backward
	 * 
	 * @param way to check the direction
	 * @return direction the direction as Integer
	 */
	private static int getWayDirection(Way way) {
		int direction = 0;

		for (int i = 0; i < way.getNumberOfTags(); i++) {
			if (way.getTag(i).getKey().equals("onway")) {

				// check if is a forward oneway
				if (way.getTag(i).getValue().equals("true") || way.getTag(i).getValue().equals("1")
						|| way.getTag(i).getValue().equals("reversible")) {
					direction += 1;
				}

				// check if is a backward oneway
				if (way.getTag(i).getValue().equals("false") || way.getTag(i).getValue().equals("0")
						|| way.getTag(i).getValue().equals("reversible") || way.getTag(i).getValue().equals("-1")) {
					direction -= 1;
				}
			}
		}
		return direction;
	}

	/**
	 * Adds the node to the lookup and the node array if it not already exist in it
	 * 
	 * @param nodeID node to add
	 */
	public static void addNode(long nodeID) {
		if (!nodesLookUp.containsKey(nodeID)) {
			nodesLookUp.put(nodeID, new double[3]);
			nodesLookUp.get(nodeID)[0] = numberOfNodes;
			nodes.add(nodeID);
			numberOfNodes++;
		}

	}
	
	/**
	 * Adds the node to the lookup and the node array if it not already exist in it
	 * Adds also direct the latitude and longitude of the node
	 * 
	 * @param nodeID node to add
	 * @param latitide of the node
	 * @param longitude of the node
	 */
	public static void addNode(long nodeID, double latitude, double longitude) {
		if (!nodesLookUp.containsKey(nodeID)) {
			nodesLookUp.put(nodeID, new double[3]);
			nodesLookUp.get(nodeID)[0] = numberOfNodes;
			nodesLookUp.get(nodeID)[1] = latitude;
			nodesLookUp.get(nodeID)[2] = longitude;
			nodes.add(nodeID);
			numberOfNodes++;
		}

	}

	/**
	 * Calculates the weight of each edge
	 */
	private static void calcEdgeWeights() {
		for (Long[] edge : edges) {
			if (nodesLookUp.get(edge[0]) != null && nodesLookUp.get(edge[1]) != null) {
				edge[2] = distanceBetweenToNodes(nodesLookUp.get(edge[0])[1], nodesLookUp.get(edge[1])[1],
						nodesLookUp.get(edge[0])[2], nodesLookUp.get(edge[1])[2]);
			}
		}
	}

	/**
	 * Calculates the distance between to nodes in meters
	 * 
	 * @param lat1 latitude of the first node
	 * @param lat2 latitude of the second node
	 * @param lng1 longitude of the first node
	 * @param lng2 longitude of the second node
	 * @return distance in meter between the two nodes
	 */
	private static long distanceBetweenToNodes(double lat1, double lat2, double lng1, double lng2) {

		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lng2 - lng1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		return Math.round(distance);

	}

	/**
	 * Sorts the node and edge array and creates a offset array. The content of the
	 * offset arrays is the beginning of the outgoing edges of the node on the same
	 * position on the node array.
	 */
	
	private static void createOffSetArray(long[][] edges) {
		Arrays.sort(edges, new NodelIdComparator());
		nodeOffset = new int[numberOfNodes];

		long tmpNode = edges[0][0];
		nodeOffset[(int) nodesLookUp.get(tmpNode)[0]] = 0;

		for (int i = 0; i < edges.length; i++) {
			// checks if the outgoing node is different then the one before
			if (edges[i][0] != tmpNode) {

				// if yes, the edges from a another node begins
				tmpNode = edges[i][0];
				try {

					// stores the beginning index in the offset array
					int nodeIndex = (int) nodesLookUp.get(tmpNode)[0];
					nodeOffset[nodeIndex] = i;
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(tmpNode);
				}
			}
		}

	}


	/**
	 * Creates single arrays for outgoing, incoming edges and edge weight out of the
	 * edge multidimensional array
	 * 
	 * @param g
	 */
	
	private static void createFromToEdgeArrays(OSMGraph g, long[][] edges) {
		int[] fromEdges = new int[edges.length];
		int[] toEdges = new int[edges.length];
		double[] edgeWight = new double[edges.length];

		for (int i = 0; i < edges.length; i++) {
			fromEdges[i] = (int) nodesLookUp.get(edges[i][0])[0];
			toEdges[i] = (int) nodesLookUp.get(edges[i][1])[0];
			edgeWight[i] = (double) edges[i][2];
		}

		g.setEdgesFrom(fromEdges);
		g.setEdgesTo(toEdges);
		g.setEdgeWight(edgeWight);
	}
	

	/**
	 * Comparator class to sort the multidimensional edge array
	 * 
	 * @author pacs1
	 *
	 */
	private static class NodelIdComparator implements Comparator<long[]> {
		@Override
		public int compare(long[] o1, long[] o2) {
			return Long.compare(o1[0], o2[0]);
		}
	}

}
