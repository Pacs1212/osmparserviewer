package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

import de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.OSMData.OSMGraph;

/**
 * Calculates the shortest path from the start node to the target node in the
 * given graph. The used algorithm is Dijkstra and it is implemented with an
 * PriorityQueue.
 * 
 * @author pacs1
 *
 */

public class OSMDijkstra {
	private static double[] distance;
	private static int[] parent;
	private static boolean[] visited;
	private static final long DEFAULTTARGET = -1L;

	/**
	 * Calculates the shortest path from the start node to the target node in the
	 * given graph and returns the path from s to t.
	 * 
	 * @param s start node
	 * @param t target node
	 * @param g graph to use
	 * @return path from s to t
	 */
	public static ArrayList<ArrayList<Double>> calcShotestPaths(long s, ArrayList<Long> targets, OSMGraph g) {
		ArrayList<ArrayList<Double>> paths = new ArrayList<>();
		dijkstra(g, s, DEFAULTTARGET);
		for (Long t : targets) {
			if (s != t) {
				paths.add(getPathWithWeight(g, s, t));
			}
		}
		return paths;
	}

	/**
	 * Calculates the shortest path from the start node to the target node in the
	 * given graph and returns the path from s to t.
	 * 
	 * @param s start node
	 * @param t target node
	 * @param g graph to use
	 * @return path from s to t, the last value of the array is the complete path
	 *         costs
	 */
	public static ArrayList<Long> calcShotestPath(long s, long t, OSMGraph g) {
		dijkstra(g, s, t);
		return getPaht(g, s, t);
	}

	/**
	 * Calculates the shortest path from the start node to the target node in the
	 * given graph
	 */
	private static void dijkstra(OSMGraph g, long s, long t) {
		// Decelerate variables with default values and store the values from the graph
		// for performance optimization(Less cache misses).
		int numberOfEdges = g.getNumberOfEdges();
		int numberOfNodes = g.getNumberOfNodes();
		HashMap<Long, double[]> nodeLookUp = g.getNodesLookUp();
		int[] edgesFrom = g.getEdgesFrom();
		int[] edgesTo = g.getEdgesTo();
		double[] edgeWight = g.getEdgeWight();
		int[] nodeOffset = g.getNodeOffset();
		double doubleMax = Double.MAX_VALUE;
		distance = new double[numberOfNodes];
		parent = new int[numberOfNodes];
		visited = new boolean[numberOfNodes];

		// Decelerate the queue with the special comparator
		PriorityQueue<Integer> prioQueue = new PriorityQueue<>(g.getNumberOfNodes(),
				new OSMDistanceEntryComperator(distance));

		for (int i = 0; i < distance.length; i++) {
			distance[i] = doubleMax;
			visited[i] = false;
			;
		}

		// Set the distance on the start node to zero and add them top the queue
		int startNodeIndex = (int) g.getNodesLookUp().get(s)[0];
		distance[startNodeIndex] = 0.0;
		prioQueue.add(startNodeIndex);

		// Run the loop until the queue is not empty
		while (!prioQueue.isEmpty()) {
			// get the node with the smallest distance in the queue and remove them
			startNodeIndex = prioQueue.poll();
			if (t != DEFAULTTARGET) {
				if (startNodeIndex == (int) nodeLookUp.get(t)[0]) {
					return;
				}
			}

			// skip if the node was already visited
			if (visited[startNodeIndex] == true) {
				continue;
			}
			visited[startNodeIndex] = true;

			// get the start index of the edges from the node offset array
			int edgeIndex = nodeOffset[startNodeIndex];

			// for each outgoing edge of this node do
			while (edgeIndex < numberOfEdges && edgesFrom[edgeIndex] == startNodeIndex) {
				int targetNodeIndex = edgesTo[edgeIndex];

				// skip if the target node was already visited
				if (visited[targetNodeIndex] == true) {
					edgeIndex++;
					continue;
				}

				double tmpWeight = distance[startNodeIndex] + edgeWight[edgeIndex];

				// checks if the new way is cheaper then the current one
				if (tmpWeight < distance[targetNodeIndex]) {
					// if yes set the distance and parent node new
					distance[targetNodeIndex] = tmpWeight;
					parent[targetNodeIndex] = startNodeIndex;
					prioQueue.add(targetNodeIndex);
				}
				edgeIndex++;
			}

		}
	}

	/**
	 * Return the shortest path from the start to the target node in the given
	 * graph.
	 * 
	 * @param g graph to search in
	 * @param s start node
	 * @param t target node
	 * @return shortest path from s to t. The the array starts from the target node
	 */
	private static ArrayList<Long> getPaht(OSMGraph g, long s, long t) {
		ArrayList<Long> path = new ArrayList<>();

		path.add(t);
		long currentNode = t;
		int currentNodeIndex = (int) g.getNodesLookUp().get(t)[0];

		// ad the parent node from the current node to the path
		while (currentNode != s) {
			currentNode = g.getAllNodes()[parent[currentNodeIndex]];
			path.add(currentNode);
			currentNodeIndex = parent[currentNodeIndex];
		}

		path.add(s);
		return path;
	}

	/**
	 * Return the shortest path from the start to the target node in the given
	 * graph. Includes the path costs
	 * 
	 * @param g graph to search in
	 * @param s start node
	 * @param t target node
	 * @return shortest path from s to t. The the array starts from the target node,
	 *         the last value is the path weight
	 */
	private static ArrayList<Double> getPathWithWeight(OSMGraph g, long s, long t) {
		ArrayList<Double> path = new ArrayList<>();

		path.add((double) t);
		long currentNode = t;
		int currentNodeIndex = (int) g.getNodesLookUp().get(t)[0];
		int lastNodeIndex = currentNodeIndex;
		double weight = 0;
		// ad the parent node from the current node to the path
		while (currentNode != s) {
			currentNode = g.getAllNodes()[parent[currentNodeIndex]];
			path.add((double) currentNode);
			currentNodeIndex = parent[currentNodeIndex];
			double tmpWeight = g.getOneEdgeWeight(currentNodeIndex, lastNodeIndex);
			weight += tmpWeight;
			lastNodeIndex = currentNodeIndex;
		}
		path.add(weight);
		return path;
	}

}
