package de.uni.stuttgart.fmi.algo.fapra.osm.OSMSpring.Navigation;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Solves the TSP for a given hamiltonian graph. The solution is calculated with
 * dynamic programming. The function to solve the TSP are inspired on the
 * function which are described in the Paper:
 * https://github.com/evandrix/SPOJ/blob/master/DP_Main112/Solving-Traveling-Salesman-Problem-by-Dynamic-Programming-Approach-in-Java.pdf
 * 
 * @author pacs1
 */
public class OSMTSP {

	private double[][] g;
	private int startNode;

	public OSMTSP(double[][] g, int startNode) {
		this.g = g;
		this.startNode = startNode;
	}

	/**
	 * Starts the calculations for the solution of the TSP
	 * 
	 * @param g         graph to calculate on
	 * @param startNode start node
	 * @return
	 */
	public ArrayList<Double> solveTSP() {
		ArrayList<Integer> setS = new ArrayList<Integer>();

		// create the start set
		for (int i = 0; i < g.length; i++) {
			if (i != startNode) {
				setS.add(i);
			}
		}

		// start the TSP calculation
		ArrayList<Double> tspPath = functionF(startNode, setS);
		tspPath.remove(0);
		return tspPath;
	}

	/**
	 * Solves the TSP with dynamic programming.
	 * 
	 * @param node current node to check
	 * @param setS set of nodes which want to be visited
	 * @return The cost of the way at the first index and the tore so fare order in
	 *         the rest of the array
	 */
	private ArrayList<Double> functionF(int node, ArrayList<Integer> setS) {
		double min = Double.MAX_VALUE;
		ArrayList<Double> tspPath = new ArrayList<Double>();

		// Checks for every node combination in the set which are the fastest way
		// between them
		for (int i = 0; i < setS.size(); i++) {
			ArrayList<Integer> smallerSetS = new ArrayList<Integer>(setS);
			int currentNode = setS.get(i);
			smallerSetS.remove(smallerSetS.indexOf(currentNode));

			// get the edge weight from the node to the current node on the set
			double edgeWeight = g[node][currentNode];

			// checks the cheapest tour from the rest of the set
			ArrayList<Double> tmpTspPath = functionF(currentNode, smallerSetS);

			// get the cost of the tour of the smaller set
			double subFuncWeight = tmpTspPath.get(0);
			double currentWeight = edgeWeight + subFuncWeight;

			// if the tour of the current node is cheaper then the tour before set this as
			// new cheapest tour
			if (currentWeight < min) {
				min = currentWeight;
				tspPath = tmpTspPath;
			}
		}

		// Returns the costs the the start node if the set was empty
		if (min == Double.MAX_VALUE) {
			return new ArrayList<Double>(Arrays.asList(g[node][startNode], (double) node));
		} else {
			tspPath.set(0, min);
			tspPath.add((double) node);
			return tspPath;
		}

	}

}
