//<![CDATA[
//Vaihingen    
//var mymap = L.map('mapid').setView([48.745, 9.104], 13);


// 51.0140481, 10.1212626
var mymap = L.map('mapid').setView([51.0140 ,10.1212], 7);
var lat;
var lng;
var fromNode;
var toNode;
var fromMarker;
var toMarker;
var tagList = [];
var barList = [];
var lineLayerList = [];
var barMarkerList = [];
var boundeBarMarkerList = [];
var barNameList = [];
var pathLayerList = [];
var barPathLayerList = [];
var graphImportet = false;
var beerIcon = L.icon({
    iconUrl: '/js/leaflet/images/Beer_Mug_small.png',
    iconSize: [45, 45] // size of the icon
});





L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(mymap);

mymap.on('moveend', scrollFunction);
mymap.on('contextmenu', onMapClick);
mymap.on('click', hideContextMenu);
document.getElementById('fileImport').addEventListener('change', handleFileSelect, false);
document.getElementById('showEdgesCheckbox').addEventListener('change', handleEdgeCheckbox);
document.getElementById('showBarsCheckbox').addEventListener('change', handleMarkerCheckbox);

function handleEdgeCheckbox() {
    clearShownEdgesOnMap();
    if (document.getElementById('showEdgesCheckbox').checked) {
        drawBoundesEdges();
    }
}

function handleMarkerCheckbox() {
    clearShownBarMarkerOnMap();
    if (document.getElementById('showBarsCheckbox').checked) {
        drawBoundesMarkers();
    }
}

function scrollFunction() {
    handleEdgeCheckbox();
    handleMarkerCheckbox();
}

function drawBoundesEdges() {
    bounds = mymap.getBounds();
    getEdgesInViewURL = "getEdgesFromCoordinates?west=" + bounds.getWest() + "&east=" + bounds.getEast() + "&north=" + bounds.getNorth() + "&south=" + bounds.getSouth();
    clearShownEdgesOnMap();
    $.get(getEdgesInViewURL, function(obj) {
        drawEdges(obj, '#ff0000');
    });
}

function drawBoundesMarkers() {
    bounds = mymap.getBounds();
    getBarsInViewURL = "getBarsInBound?west=" + bounds.getWest() + "&east=" + bounds.getEast() + "&north=" + bounds.getNorth() + "&south=" + bounds.getSouth();
    clearShownBarMarkerOnMap();
    $.get(getBarsInViewURL, function(bars) {
        bars.forEach(drawBarMarker);
    });
}


function handleFileSelect(evt) {
    alert('Import graph data please wait...');
    var fullPath = document.getElementById('fileImport').value;
    if (fullPath) {
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        var fileName = fullPath.substring(startIndex);
        if (fileName.indexOf('\\') === 0 || fileName.indexOf('/') === 0) {
            fileName = fileName.substring(1);
        }
    }
    getGraphURL = "getImportGraph?fileName=" + fileName;
    $.get(getGraphURL, function() {
        alert('Graph import finished');
        graphImportet = true;
    });

}

function drawBarMarker(bar) {
    var marker = L.marker([bar[0], bar[1]], { icon: beerIcon }).addTo(mymap);
    marker.bindPopup(bar[2]).openPopup();
    boundeBarMarkerList.push(marker);
}

function drawEdges(edges, lineColor) {
    edges.forEach(function(obj) {
        var line = L.polyline([
            [obj[0], obj[1]],
            [obj[2], obj[3]],
        ], {
            color: lineColor
        }).addTo(mymap);
        mymap.addLayer(line);
        lineLayerList.push(line);
    });
}

function drawPath(edges, lineColor) {
    edges.forEach(function(obj) {
        var line = L.polyline([
            [obj[0], obj[1]],
            [obj[2], obj[3]],
        ], {
            color: lineColor
        }).addTo(mymap);
        mymap.addLayer(line);
        pathLayerList.push(line);
    });
}

function drawBarPath(edges, lineColor) {
    edges.forEach(function(obj) {
        var line = L.polyline([
            [obj[0], obj[1]],
            [obj[2], obj[3]],
        ], {
            color: lineColor
        }).addTo(mymap);
        mymap.addLayer(line);
        barPathLayerList.push(line);
    });
}

function clearWholeMap() {
    lineLayerList.forEach(function(layer) {
        mymap.removeLayer(layer);
    });
    pathLayerList.forEach(function(layer) {
        mymap.removeLayer(layer);
    });
    lineLayerList = [];
    pathLayerList = [];
}

function clearShownBarPathOnMap() {
    barPathLayerList.forEach(function(layer) {
        mymap.removeLayer(layer);
    });
    barPathLayerList = [];
}

function clearShownEdgesOnMap() {
    lineLayerList.forEach(function(layer) {
        mymap.removeLayer(layer);
    });
    lineLayerList = [];
}

function clearShownBarMarkerOnMap() {
    boundeBarMarkerList.forEach(function(layer) {
        mymap.removeLayer(layer);
    });
    boundeBarMarkerList = [];
}

function setFromHere() {
    getNodeURL = "getNearestNode?lat=" + lat + "&lng=" + lng;
    $.get(getNodeURL, function(node) {
        fromNode = node;
        document.getElementById('fromText').innerText = fromNode;

        getCordFromeNodeURL = "getCoorinateFromeNode?node=" + node;
        $.get(getCordFromeNodeURL, function(coordinates) {
            removeMarker(fromMarker);
            fromMarker = L.marker([coordinates[0], coordinates[1]]).addTo(mymap);
            fromMarker.bindPopup("Navigate from here").openPopup();
            setMarker();
        });
    });

    hideContextMenu();
}

function setToHere() {
    getNodeURL = "getNearestNode?lat=" + lat + "&lng=" + lng;
    $.get(getNodeURL, function(node) {
        toNode = node;
        document.getElementById('toText').innerText = toNode;

        getCordFromeNodeURL = "getCoorinateFromeNode?node=" + node;
        $.get(getCordFromeNodeURL, function(coordinates) {
            removeMarker(toMarker);
            toMarker = L.marker([coordinates[0], coordinates[1]]).addTo(mymap);
            toMarker.bindPopup("Navigate to here").openPopup();
            setMarker();
        });

    });
    hideContextMenu();
}

function removeMarker(marker) {
    if (marker != undefined) {
        mymap.removeLayer(marker);
    }
}

function setMarker() {
    clearWholeMap();
    mymap.addLayer(toMarker);
    mymap.addLayer(fromMarker);
}

function onContainerClick(e) {
    e.preventDefault();
    var contextmenu = document.getElementById('contextmenu');
    contextmenu.style.visibility = "visible";
    contextmenu.style.left = e.pageX;
    contextmenu.style.top = e.pageY;
    return false;
}

function hideContextMenu() {
    document.getElementById('contextmenu').style.visibility = "hidden";
}

function onMapClick(e) {
    lat = e.latlng.lat;
    lng = e.latlng.lng;
}

function calculateRoute() {
    getPathURL = "getShortestPath?start=" + fromNode + "&target=" + toNode;
    clearWholeMap();
    $.get(getPathURL, function(obj) {
        drawPath(obj, '#004C00');
    });
}

function calculateBarRoute() {
    deletAllTags();
    clearShownBarPathOnMap();
    getShortestBarTourURL = "calcShortestBarTour?bars=" + barList;
    $.get(getShortestBarTourURL, function(barRoute) {
        drawBarPath(barRoute, '#004C00');
        var barCounter = 0;
        getShortestBarTourOrderURL = "getBarToureOrder?startBar=" + barList[0];
        $.get(getShortestBarTourOrderURL, function(barNames) {
            barNames.forEach(function(barName) {
                addTagToList(barName, barCounter);
                barCounter = barCounter + 1;
            });
        });
    });
}

function addBar() {
    clearShownBarPathOnMap();
    deletAllTags();
    getBarURL = "getNearestBar?lat=" + lat + "&lng=" + lng;
    $.get(getBarURL, addBarToList);
}

function addBarToList(bar) {
    var barID = bar;
    getBarNameURL = "getLocationName?node=" + barID;

    $.get(getBarNameURL, function(obj) {
        var barName = obj[0];
        if (barNameList.indexOf(barName) > -1) {
            alert("Bar alredy added");
            return;
        }
        barNameList.push(barName);
        barList.push(barID);
        var ul = document.getElementById("barList");
        var deleteButton = document.createElement('button');

        deleteButton.setAttribute('onclick', 'deleteBarFromList(' + barID + ')');
        deleteButton.appendChild(document.createTextNode('X'));

        var li = document.createElement("li");
        li.setAttribute('id', barID);
        li.appendChild(document.createTextNode(barName));
        li.appendChild(deleteButton)
        ul.appendChild(li);

        createBarIcon(barName, barID);

    });
    hideContextMenu();
}

function createBarIcon(barName, barID) {
    getCordFromeNodeURL = "getCoorinateFromeNode?node=" + barID;
    $.get(getCordFromeNodeURL, function(coordinates) {
        var marker = L.marker([coordinates[0], coordinates[1]], { icon: beerIcon }).addTo(mymap);
        marker.bindPopup(barName).openPopup();
        barMarkerList.push(marker);
    });
}

function clearBarList() {
    clearShownBarPathOnMap();
    var ul = document.getElementById("barList");
    barList.forEach(function(bar) {
        var item = document.getElementById(bar);
        ul.removeChild(item);
    });

    barMarkerList.forEach(function(bar) {
        removeMarker(bar);
    });
    clearWholeMap();
    barList = [];
    barNameList = [];
    barMarkerList = [];
}

function deleteBarFromList(barId) {
    clearShownBarPathOnMap();
    deletAllTags();
    var ul = document.getElementById("barList");
    var item = document.getElementById(barId);
    ul.removeChild(item);

    barIndex = barList.indexOf(barId);
    if (barIndex > -1) {
        barList.splice(barIndex, 1);
        barNameList.splice(barIndex, 1);
        var tmpMaker = barMarkerList[barIndex];
        removeMarker(tmpMaker);
        barMarkerList.splice(barIndex, 1);

    }
}

function addTagToList(tag, idCount) {
    tagList.push(idCount);

    var ul = document.getElementById("tagList");

    var li = document.createElement("li");
    li.setAttribute('id', idCount);
    li.appendChild(document.createTextNode(idCount + ": " + tag));
    ul.appendChild(li);
}

function deletAllTags() {
    var ul = document.getElementById("tagList");

    tagList.forEach(function(tagID) {
        console.log("tagID " + tagID);
        var item = document.getElementById(tagID);
        ul.removeChild(item);
    });

    tagList = [];
}

//]]>